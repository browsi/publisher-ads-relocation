const webpack = require("webpack");

module.exports = [
    {
        entry: {
            "par": ["./src/app.ts"]
        },
        output: {
            path: __dirname,
            filename: "[name].js"
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    use: 'ts-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.ts$/,
                    enforce: 'pre',
                    loader: 'tslint-loader',
                    options: {
                        configuration: {
                            rules: {
                                // quotemark: [true, 'single'],
                                typedef: [true,
                                    "call-signature",
                                    "parameter",
                                    "member-variable-declaration",
                                    "arrow-call-signature"
                                ]
                                // "no-any": true
                            }
                        },
                        failOnHint: true,
                        typeCheck: false

                    }
                }
            ]
        },
        resolve: {
            extensions: [".ts", ".js"]
        },
        watch: !process.argv.includes('--no-watch'),
        plugins: [
            new webpack.optimize.UglifyJsPlugin({
                include: /\.min\.js$/,
                minimize: true,
                mangle: true
            })
        ]
    }
];
