const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: {
        par: './par.js'
    },
    plugins: [
        new UglifyJsPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.ts$/,
                enforce: 'pre',
                loader: 'tslint-loader',
                options: {
                    configuration: {
                        rules: {
                            quotemark: [true, 'double'],
                            "no-debugger": true
                        }
                    },
                    failOnHint: true,
                    typeCheck: false

                }
            }
        ]
    },
    resolve: {
        extensions: [ ".tsx", ".ts", ".js" ]
    },
    output: {
        filename: '[name].min.js',
    }
};