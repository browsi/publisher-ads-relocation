import GeneralSize = googletag.GeneralSize;
import Slot = googletag.Slot;
import {Logger} from "./helpers/Logger";
import {DefinedSlot, PubadsObject} from "./models/PubadsObject";

declare let window: any;

export class PubAdsRelocator {

    private definedSlots: DefinedSlot[];
    private pubAdsObject: PubadsObject;
    private logger: Logger;

    // @ts-ignore
    private oldGptDefineSlot: (au: string, s: GeneralSize, cn?: string) => Slot;
    // @ts-ignore
    private oldGptDisplay: (cn: string) => void;
    // @ts-ignore
    private oldGptRefresh: (slots?: Slot[], options?: {changeCorrelator: boolean}) => void;
    private pbjsInstanceName: string;

    constructor(pbjsVarName: string) {
        this.pbjsInstanceName = pbjsVarName;
        this.definedSlots = [];
        this.logger = new Logger("PubAdsRelocator");

        this.pubAdsObject = {
            definedSlots: this.definedSlots,
            startedSpotEngine: false,
            filterSlots: this.filterSlots.bind(this),
            releaseAllSlots: this.releaseAllSlots.bind(this)
        };
        window.top['browsitag'] = Object.assign(window.top['browsitag'] || {}, {browsiPubAds: this.pubAdsObject});
    }

    public saveOldGptMethods(): void {
        this.oldGptDefineSlot = googletag.defineSlot;
        this.oldGptDisplay = googletag.display;
        this.oldGptRefresh = googletag.pubads().refresh;
    }

    public subscribePrebidAdUnits(triggerReqBids: any, reqBidsConfigObj: any): void {
        if (this.shouldCollectAdUnits()) {
            const adUnitCodes = reqBidsConfigObj.adUnitCodes || reqBidsConfigObj.adUnits && reqBidsConfigObj.adUnits.map((au: any) => au.code) || window.top[this.pbjsInstanceName].adUnits.map((au: any) => au.code);
            const prebidSlots = this.definedSlots.filter(conf => adUnitCodes.includes(conf.containerName));
            let reqBidsNonAuParts = Object.assign({}, reqBidsConfigObj);
            if (reqBidsConfigObj.adUnits) {
                delete reqBidsNonAuParts.adUnits;
            }
            prebidSlots.forEach((definedSlot: DefinedSlot) => {
                definedSlot.isPrebid = true;
                const reqBidsNewConfig = Object.assign({}, reqBidsNonAuParts, {adUnitCodes: [definedSlot.containerName]});
                definedSlot.display = (): void => {
                    this.logger.info(`Called requestBids for: `, reqBidsNewConfig);
                    definedSlot.displayTriggered = true;
                    triggerReqBids(reqBidsNewConfig);
                };
            });
        }
        else {
            triggerReqBids(reqBidsConfigObj);
        }
    }

    public gptDefineSlot(adUnit: string, size: GeneralSize, containerName?: string): Slot {
        const slot = this.oldGptDefineSlot(adUnit, size, containerName);
        if (this.shouldCollectAdUnits()) {
            this.logger.info(`Pushing adUnit: ${adUnit}, size: ${size}, containerName: ${containerName}`);
            const newConfig = {
                adUnit: adUnit,
                size: size,
                containerName: containerName,
                slot: slot,
                isPrebid: false,
                displayTriggered: false,
                // todo: think about how to handle undefined containerName
                display: (): void => {
                    newConfig.displayTriggered = true;
                    this.logger.info(`Called oldGptDisplay( ${containerName} )`);
                    this.oldGptDisplay(containerName || '');
                    if ( (<any>googletag.pubads())['isInitialLoadDisabled']() ) {
                        this.logger.info(`Called oldGptRefresh( ${containerName} )`);
                        this.oldGptRefresh([slot]);
                    }
                },
                refresh: (): void => {
                    this.logger.info('refreshing slot: ', containerName);
                    this.oldGptRefresh([slot]);
                }
            };
            this.definedSlots.push(newConfig);
        }
        return slot;
    }

    public gptDisplay(containerName: string): void {
        if (!this.shouldCollectAdUnits() || this.isPrebidContainer(containerName)) {
            this.oldGptDisplay(containerName);
        } else {
            this.logger.info(`Tried to call googletag.display( ${containerName} ), but blocked`);
        }
    }

    public gptRefresh(slots?: Slot[], options?: {changeCorrelator: boolean}): void {
        if ( !this.shouldCollectAdUnits() ) {
            if (slots) {
                slots.forEach((slot: Slot) => {
                    const prebidSlot = this.definedSlots.find((definedSlot: DefinedSlot) => definedSlot.isPrebid && definedSlot.slot === slot);
                    if (prebidSlot && prebidSlot.displayTriggered) {
                        this.logger.info(`Called googletag.pubads().refresh( ${prebidSlot.containerName} )`);
                        this.oldGptRefresh([prebidSlot.slot], options);
                    } else if (prebidSlot) {
                        this.logger.info(`Tried to call googletag.pubads().refresh( ${prebidSlot.containerName} ), but blocked`, prebidSlot)
                    } else {
                        this.logger.info(`Called googletag.pubads().refresh()`);
                        this.oldGptRefresh(slots, options);
                    }
                });
            } else {
                this.logger.info(`Called googletag.pubads().refresh()`);
                this.oldGptRefresh(slots, options);
            }
        } else {
            this.logger.info(`Tried to call googletag.pubads().refresh( ${slots || ''} ), but blocked`, slots);
        }
    }

    public filterSlots(loaderParams: any): void {
        this.removeBlacklisted([], 'adUnit');
        this.removeBlacklisted([], 'containerName');
        this.removeNonWhitelisted([], 'adUnit');
        this.removeNonWhitelisted([], 'containerName');
        this.pubAdsObject.definedSlots = this.definedSlots;
    }

    public releaseAllSlots(): void {
        this.pubAdsObject.startedSpotEngine = true;
        this.definedSlots.forEach((slot: DefinedSlot) => {
            this.logger.info(`Slot: ${slot.adUnit}, ${slot.containerName} location isn't changed. Displaying it...`);
            slot.display();
        });
        this.definedSlots = [];
    }

    private removeBlacklisted<Key extends keyof DefinedSlot>(blacklistAdUnits: string[], key: Key): void {
        if (blacklistAdUnits.length > 0 && this.definedSlots.length > 0) {
            this.definedSlots.forEach((slot: DefinedSlot) => {
                if (blacklistAdUnits.includes(<string> slot[key])) {
                    this.logger.info(`Slot: ${slot.adUnit}, ${slot.containerName} is excluded. Displaying it...`);
                    slot.display();
                }
            });
            this.definedSlots = this.definedSlots.filter((slot: DefinedSlot) => !blacklistAdUnits.includes(<string> slot[key]));
        }
    }

    private removeNonWhitelisted<Key extends keyof DefinedSlot>(whitelist: string[], key: Key): void {
        if (whitelist.length > 0 && this.definedSlots.length > 0) {
            this.definedSlots.forEach((slot: DefinedSlot) => {
                if (!whitelist.includes(<string> slot[key])) {
                    this.logger.info(`Slot: ${slot.adUnit}, ${slot.containerName} is excluded. Displaying it...`);
                    slot.display();
                }
            });
            this.definedSlots = this.definedSlots.filter((slot: DefinedSlot) => whitelist.includes(<string> slot[key]));
        }
    }

    private isPrebidContainer(containerName: string): boolean {
        return !!this.definedSlots.find(definedSlot => definedSlot.isPrebid && definedSlot.containerName === containerName);
    }

    private shouldCollectAdUnits(): boolean {
        return !this.pubAdsObject.startedSpotEngine;
    }
}