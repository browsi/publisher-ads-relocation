import GeneralSize = googletag.GeneralSize;
import Slot = googletag.Slot;

export interface PubadsObject {
    definedSlots: Array<DefinedSlot>;
    startedSpotEngine: boolean;
    releaseAllSlots: () => void;
    filterSlots: (loaderParams: any) => void;
}

export interface DefinedSlot {
    adUnit: string;
    size: GeneralSize;
    containerName?: string;
    display: () => void;
    refresh: () => void;
    slot: Slot;
    isPrebid: boolean;
    displayTriggered: boolean;
}

export interface PubadsFilter {
    whiteList: FilterList;
    blackList: FilterList;
}

declare interface FilterList {
    adUnit: string[];
    containerName: string[];
}