export interface TagData {
    /**pubads relocation*/
    par: boolean;
    /**pbjs instance name*/
    pb: string;
}
