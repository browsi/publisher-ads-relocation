import {PubAdsRelocator} from "./PubAdsRelocator";
import {TagData} from "./models/TagData";
import Utils from "./core/Utils";
import {Logger} from "./helpers/Logger";

declare let window: any;

export class MainApp {

    private readonly startTime: number;
    // @ts-ignore
    private pubAdsRelocator: PubAdsRelocator;
    private readonly tagData: TagData;
    private readonly debugConfiguration: any;
    private readonly logger: Logger;

    constructor() {
        this.startTime = +new Date();
        this.tagData = Utils.getTagData();
        this.debugConfiguration = Utils.getD();
        this.logger = new Logger('PubadsRelocationApp');
    }

    run(): void {
        this.tunnelPublisherAds();
    }

    private tunnelPublisherAds(): void {
        const pbjsInstanceName: any = this.tagData.pb;
        this.pubAdsRelocator = new PubAdsRelocator(pbjsInstanceName);
        window.top[pbjsInstanceName] = window.top[pbjsInstanceName] || {};
        window.top[pbjsInstanceName].que = window.top[pbjsInstanceName].que || [];
        window.top[pbjsInstanceName].que.push((): void => {
            this.logger.info("entered overriding pbjs");
            if (this.debugConfiguration.debug === true) {
                window.top[pbjsInstanceName].setConfig({debug: true});
            }
            window.top[pbjsInstanceName].requestBids.before(
                (triggerReqBids: any, reqBidsConfigObj: any) => {
                    this.pubAdsRelocator.subscribePrebidAdUnits(triggerReqBids, reqBidsConfigObj);
                }, 50);
        });

        const googleTag = Utils.getGoogletag();
        let newQueue = [];
        newQueue.push((): void => {
            this.pubAdsRelocator.saveOldGptMethods();
            googleTag.defineSlot = this.pubAdsRelocator.gptDefineSlot.bind(this.pubAdsRelocator);
            googleTag.display = this.pubAdsRelocator.gptDisplay.bind(this.pubAdsRelocator);
            googleTag.pubads().refresh = this.pubAdsRelocator.gptRefresh.bind(this.pubAdsRelocator);
        });
        for (let i = 0; i < (<any>googleTag.cmd).length; i++) {
            newQueue.push((<any>googleTag.cmd)[i]);
        }
        googleTag.cmd = newQueue;
        this.logger.info('changed gpt: ', googletag.cmd);
    }
}

new MainApp().run();