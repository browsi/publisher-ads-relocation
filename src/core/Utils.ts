import Googletag = googletag.Googletag;
import {TagData} from "../models/TagData";

export default class Utils {
    private static googleTag: Googletag;

    static getTagData(): TagData {
        let tag;
        try {
            tag = document.currentScript;
        } catch (e) {
            tag = document.getElementById('browsi-relocate');
        }
        if (!tag) {
            throw new Error('no tag');
        }
        return {
            par: (tag.hasAttribute('data-relocate') && !!tag.getAttribute('data-relocate')),
            pb: tag.getAttribute('data-pbjs') || 'pbjs',
        }
    }

    static getSessionStorage(key:string):string{
        let value;
        try {
            value = sessionStorage.getItem(key)
        } catch (e) {

        }
        return value || '';
    }

    static getD(): any {
        const __brwsidbg = this.getSessionStorage('__brwsidbg');
        let result = {};
        if (__brwsidbg) {
            result = JSON.parse(__brwsidbg);
        }
        return result;
    }

    static getGoogletag(): Googletag {
        if (!Utils.googleTag || !Utils.googleTag.cmd) {
            (<any>window).top.googletag = (<any>window).top.googletag || {};
            (<any>window).top.googletag.cmd = (<any>window).top.googletag.cmd || [];
            Utils.googleTag = (<any>window).top.googletag;
        }
        return Utils.googleTag;
    }
}