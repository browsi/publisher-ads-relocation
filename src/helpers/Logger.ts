import Utils from '../core/Utils';

export const enum LogTypes {
    INFO = 'info',
    ERROR = 'error',
    CUSTOM = 'custom',
    DEBUG = 'debug'
}

export class Logger{

    private className: string;
    private isActive: boolean;
    constructor(className: string) {
        this.className = className;
        this.isActive = !!(Utils.getD().debug);
    }

    public info(...data: any[]): void {
        this.logMessage(LogTypes.INFO,data);
    }

    public verbose(...data: any[]): void {
        this.logMessage(LogTypes.DEBUG,data);
    }

    public custom(msg: string, style: string, ...args: any[]): void {
        const data =  [{
            msg: msg,
            style,
            args
        }];

        this.logMessage(LogTypes.CUSTOM,data);
    }

    public error( ...data: any[]): void {
        this.logMessage(LogTypes.ERROR,data);
    }

    private logMessage(messageType: LogTypes, data: any[]): void {
        if(!this.isActive) {
            return;
        }
        if(messageType !== LogTypes.CUSTOM) {
            console[messageType](`[${this.className}]`, ...data);
        } else {
            console.info(`%c[${this.className}] ${data[0].msg}`, data[0].style, data[0].args);
        }
    }
}
